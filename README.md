# dotfiles

My personal dotfiles.


## Installation

```
git clone https://gitlab.com/apahne/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
~/.dotfiles/install.sh
```


## Inspiration

* https://github.com/webpro/dotfiles
* https://github.com/JensErat/pandoc-scrlttr2 for panletter
* https://dotfiles.github.io/



