
do_stow(){
    stow -v "$1"
}

clean(){

    if [ -h "$1" ]; then
        # remove symlink
        echo "removing symlink $1"
        rm "$1"
        return
    fi
    if [ -d "$1" ]; then
        # move directory
        backupdir="$1.$(date '+%Y-%m-%d-%H-%M-%S').bak"
        echo "removing $1" - backup: "$backupdir"
        mv "$1" "$backupdir"
        return
    fi
    if [ -f "$1" ]; then
        # backup and then remove regular file
        backupfile="$1.$(date '+%Y-%m-%d-%H-%M-%S').bak"
        echo "removing $1" - backup: "$backupfile"
        mv "$1" "$backupfile"
        return
    fi

}

add_path(){
    if [[ $PATH != *"$1"* ]] ; then
        PATH="$1":${PATH}
    fi
}

add_path_if_exists(){
    if [ -d "$1" ]; then
        if [[ $PATH != *"$1"* ]] ; then
            PATH="$1":${PATH}
        fi
    fi
}


initialize_vim_vundle(){
    vundle_home="$HOME/.vim/bundle/vundle"
    if [[ ! -d "$vundle_home" ]]; then
    git clone https://github.com/VundleVim/Vundle.vim.git $vundle_home
    fi
}
