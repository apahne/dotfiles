set nocompatible  " next millenium

" Vundle
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Plugins
Plugin 'gmarik/vundle'

Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-vinegar'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'tpope/vim-repeat'
Plugin 'w0rp/ale'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'NLKNguyen/papercolor-theme'

" after vundle initialization, we can go back to normal
filetype plugin on
filetype indent on


"  Theme & Colors
syntax on       " enable syntax processing

" load the color scheme before anything
set background=light
colorscheme PaperColor
let g:airline_theme='papercolor'

set lazyredraw       " redraw only when we need to - speed up some operations
set autoread		" Set to auto read when a file is changed from the outside
set hidden

" backups
set backup
set writebackup
set backupdir=$HOME/.vim-storage/backup//

" swap file
set swapfile
set directory=$HOME/.vim-storage/swap//

" undo
set undofile " permanent undo history
set undodir=$HOME/.vim-storage/undo//


"  Editing
"

set encoding=utf8                  " utf8 everywhere
set fileencoding=utf-8
set backspace=indent,eol,start     " backspace handling
set autoindent                     " always auto indent
set showmatch                      " highlight matching [{()}]

" do not continue comment on subsequent lines
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o


"  Spaces & Tabs
set smarttab
set expandtab        " tabs are spaces
set tabstop=4        " number of spaces in tab when editing
set shiftwidth=4     " number of spaces in tab when editing
set softtabstop=4    " number of spaces in tab when editing


" Line Wrapping
set wrap      		 " wrap long lines
set linebreak
set nolist


" Files
"
set path+=**        " search down into subfolders; provides tab-completion
                    " for file related tasks
set wildmenu        " display matching files when tab completing
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.iml,*/target/*


" fzf - fuzzy everything
"

set rtp+=~/.fzf     " add fzf to runtime path

" fix fzf not honoring the color scheme
" https://github.com/junegunn/fzf.vim/issues/59
function! s:update_fzf_colors()
  let rules =
  \ { 'fg':      [['Normal',       'fg']],
  \ 'bg':      [['Normal',       'bg']],
  \ 'hl':      [['Comment',      'fg']],
  \ 'fg+':     [['CursorColumn', 'fg'], ['Normal', 'fg']],
  \ 'bg+':     [['CursorColumn', 'bg']],
  \ 'hl+':     [['Statement',    'fg']],
  \ 'info':    [['PreProc',      'fg']],
  \ 'prompt':  [['Conditional',  'fg']],
  \ 'pointer': [['Exception',    'fg']],
  \ 'marker':  [['Keyword',      'fg']],
  \ 'spinner': [['Label',        'fg']],
  \ 'header':  [['Comment',      'fg']] }
let cols = []
for [name, pairs] in items(rules)
  for pair in pairs
    let code = synIDattr(synIDtrans(hlID(pair[0])), pair[1])
    if !empty(name) && code > 0
      call add(cols, name.':'.code)
      break
    endif
  endfor
endfor
let s:orig_fzf_default_opts = get(s:, 'orig_fzf_default_opts', $FZF_DEFAULT_OPTS)
let $FZF_DEFAULT_OPTS = s:orig_fzf_default_opts .
      \ empty(cols) ? '' : ('--color='.join(cols, ',')) .
      \ " --layout=reverse --inline-info"
endfunction

augroup _fzf
  autocmd!
  "  autocmd ColorScheme * call <sid>update_fzf_colors()
  autocmd VimEnter,ColorScheme * call s:update_fzf_colors()
augroup END


"  Searching
"
set ignorecase
set smartcase
set incsearch       " search as characters are entered
set hlsearch        " highlight matches

" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor
endif

" n to always search forward and N backwards regardless whether / or ? was used
nnoremap <expr> n  'Nn'[v:searchforward]
xnoremap <expr> n  'Nn'[v:searchforward]
onoremap <expr> n  'Nn'[v:searchforward]
nnoremap <expr> N  'nN'[v:searchforward]
xnoremap <expr> N  'nN'[v:searchforward]
onoremap <expr> N  'nN'[v:searchforward]


"  Editor UI & Behaviour
"

set noerrorbells    " no sound on error
set novisualbell    " no sound on error
set t_vb=           " no sound on error
set tm=500          " no sound on error
set cursorline      " highlight line under cursor

" Line Numbers
set number                  " show line numbers
set number relativenumber   " relative line numbers

" relative line numbers only in normal mode, switch to absolute numbers in edit mode
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" goto last edit location when opening any file (except git commit messages)
autocmd BufReadPost *
    \ if expand('%:p') !~# '\m/\.git/' && line("'\"") > 0 && line("'\"") <= line("$") |
    \     exe "normal! g`\"" |
    \ endif

" Change cursor shape to block in normal mode
if empty($TMUX)
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
  let &t_SR = "\<Esc>]50;CursorShape=2\x7"
else
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
  let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
endif


" no line numbers in terminal buffers
augroup terminal
    au!
    autocmd TermOpen * setlocal nonumber norelativenumber
augroup END


"  Key Mappings
"
set timeoutlen=1200

" leader key is SPACE and ,
nnoremap <SPACE> <Nop>
let g:mapleader = "\<Space>"
let g:maplocalleader = ','


" leader "," mappings
nnoremap <localleader>w :w!<cr>
nnoremap <localleader>q :wq!<cr>
nnoremap <leader>q :wq!<cr>

" Spacemacs Inspired - <SPACE>
nnoremap <leader><tab> :b#<cr>
nnoremap <leader><Space> :History:<cr>

" Spacemacs Inspired - <Space> f - file(s)
nnoremap <leader>ff :Files<cr>
nnoremap <leader>pf :Files<cr>
nnoremap <leader>fs :w!<cr>
nnoremap <leader>fr :History<cr>
nnoremap <leader>feR :source ~/.vimrc<cr>

" Spacemacs Inspired - <Space> a - search
nnoremap <leader>ss :BLines<cr>

" Spacemacs Inspired - <Space> b - buffers(s)
nnoremap <leader>bb :Buffers<cr>
nnoremap <leader>bs :w!<cr>
nnoremap <leader>bd :bdelete<cr>

" Spacemacs Inspired - <Space> w - window(s)
nnoremap <leader>ww :Windows<cr>

" yank (paste) complete buffer into (from) clipboard
nnoremap <silent> <leader>bY mqgg"+yG`q
nnoremap <silent> <leader>bP ggdG"+PG

" execute current buffer as shell script
" no <cr>, so that parameters can be added
nnoremap <leader>bx :!"%:p"
nnoremap <leader>bX :!"%:p"<cr>

" re-use German umlaute
"
exe "noremap <Char-196> )" | " Ä
exe "noremap <Char-214> (" | " Ö
exe "noremap <Char-228> }" | " ä
exe "noremap <Char-246> {" | " ö

" natural navigation in wrapped longs lines
noremap <silent> <Up> gk
imap <silent> <Up> <C-o>gk
noremap <silent> <Down> gj
imap <silent> <Down> <C-o>gj
noremap <silent> <home> g<home>
imap <silent> <home> <C-o>g<home>
noremap <silent> <End> g<End>
imap <silent> <End> <C-o>g<End>

" move line(s) around
nnoremap <A-S-down> :m .+1<CR>==
nnoremap <A-S-up> :m .-2<CR>==
inoremap <A-S-down> <Esc>:m .+1<CR>==gi
inoremap <A-S-up> <Esc>:m .-2<CR>==gi
vnoremap <A-S-down> :m '>+1<CR>gv=gv
vnoremap <A-S-up> :m '<-2<CR>gv=gv


" saner CTRL-l
"By default, <c-l> clears and redraws the screen (like :redraw!). The following mapping does the same, plus de-highlighting the matches found via /, ? etc., plus fixing syntax highlighting (sometimes Vim loses highlighting due to complex highlighting rules), plus force updating the syntax highlighting in diff mode:
nnoremap <C-l> :nohlsearch<cr>:diffupdate<cr>:syntax sync fromstart<cr><c-l>



" do not leak credentials when using vim and gopass
" see https://github.com/gopasspw/gopass/blob/master/docs/setup.md#securing-your-editor
au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile




" at last
if filereadable(expand("~/.vimrc.local"))
    source ~/.vimrc.local
endif

