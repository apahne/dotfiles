
# If not running interactively, don't do anything
[ -z "$PS1" ] && return

[[ -d "$HOME/.shells.d" ]] && for f in "$HOME"/.shells.d/* ; do source $f; done
[[ -d "$HOME/.bash.d" ]] && for f in "$HOME"/.bash.d/* ; do source $f; done

export PS1="\u@\W$ "

. "$HOME/.locale-settings"