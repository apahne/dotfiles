# .zshenv is always sourced. It often contains exported variables that should be
# available to other programs.

cargo_env="$HOME/.cargo/env"
if [ -x "$cargo_env" ]; then
  . "$cargo_env"
fi
