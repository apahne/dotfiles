
#
#    this should happen late:
#
# Hook for extra/custom stuff
EXTRA_DIR="$HOME/.extra"
if [ -d "$EXTRA_DIR" ]; then
    if [ -d "$EXTRA_DIR/sourced" ]; then
        for EXTRAFILE in "$EXTRA_DIR"/sourced/*; do
            [ -f "$EXTRAFILE" ] && . "$EXTRAFILE"
        done
    fi
fi

