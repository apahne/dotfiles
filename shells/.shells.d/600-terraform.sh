
path_to_terraform=$(which terraform)
if [ -x "$path_to_terraform" ] ;  then
    complete -o nospace -C "$path_to_terraform" terraform
fi
