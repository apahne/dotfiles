#   PATH setup
dotfiles="$HOME/.dotfiles"
source "$dotfiles/.dotfiles-lib.bash"

add_path "$dotfiles/bin"

add_path_if_exists "$HOME/.local/bin"
add_path_if_exists "$HOME/go/bin"
add_path_if_exists "/opt/kafka/bin"
add_path_if_exists "/lib/cargo/bin"
add_path_if_exists "$HOME/miniconda3/bin"
add_path_if_exists $HOME/bin
add_path_if_exists $HOME/.cargo/bin
add_path_if_exists $HOME/.yarn/bin
add_path_if_exists $HOME/Downloads/ccloud/bin
add_path_if_exists $HOME/.local/share/JetBrains/Toolbox/scripts
add_path_if_exists /opt/confluent/bin


# only, if repo is available and unlocked
secret_dir="$HOME/.dotfiles-secret"
secret_dir_encryption_marker="$secret_dir/.encryption.marker"
if [[ -d "$secret_dir" ]]; then
    if [ -f "$secret_dir_encryption_marker" ]; then
        if grep -q "hello world" "$secret_dir_encryption_marker"; then
            add_path_if_exists "$secret_dir/bin"
        fi
    fi
fi


export PATH=${PATH}
