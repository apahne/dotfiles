
export FZF_DEFAULT_OPTS='--layout=reverse '
export FZF_DEFAULT_COMMAND='fd . --hidden --exclude ".git"'