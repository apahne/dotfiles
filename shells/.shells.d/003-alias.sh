alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."


# ls and friends
alias ll="ls -l -G"
alias la="ls -l -G -a"

alias xl="exa -l --git"
alias xa="exa -a --git"
alias xla="exa -la --git"

#  Navigation: common
#
alias ddai="cd ~/data/ai"
alias ddatrack="cd ~/data/atrack-data"
alias ddawesome="cd ~/data/awesome-free-audio"
alias ddbeats="cd ~/beats"
alias dddata="cd ~/data"
alias dddiy="cd ~/data/diy"
alias dddocuments="cd ~/Documents"
alias dddownloads="cd ~/Downloads"
alias dddotfiles="cd ~/.dotfiles"
alias dddotfilessecret="cd ~/.dotfiles-secret"
alias ddebsdorfer="cd ~/data/acts/ebsdorfer-blomberg"
alias ddfinanzamt="cd ~/data/finanzamt/2025"
alias ddfractal="cd ~/projects/fractal-presets"
alias ddhaendel="cd ~/data/acts/händel-lemgo"
alias ddhays="cd ~/data/hays/puma24"
alias ddhealth="cd ~/data/health-fitness"
alias ddhomenet="cd ~/projects/homenet"
alias ddhomenetsecret="cd ~/projects/homenet-secret"
alias ddinventory="cd ~/data/inventory"
alias ddjournal="cd ~/data/journal"
alias ddkemper="cd ~/synched/kemper-profiles"
alias ddkochbuch="cd ~/data/kochbuch"
alias ddlearning-guitar="cd ~/data/learning-guitar"
alias ddledger="cd ~/data/ledger"
alias ddnerd-stuff="cd ~/data/nerd-stuff"
alias ddnutrition="cd ~/data/health-fitness/nutrition"
alias ddmanuals="cd ~/synched/manuals"
alias ddmusic-texts="cd ~/data/music-texts"
alias ddprojects="cd ~/projects"
alias ddorg="cd ~/data/org"
alias ddpuma="cd ~/work/puma"
alias ddpumadocs="cd ~/work/puma/docs"
alias ddhaven="cd ~/work/puma/haven"
alias ddhive="cd ~/work/puma/hive"
alias ddsolcom="cd ~/data/solcom"
alias ddsnippets="cd ~/data/snippets"
alias ddsynched="cd ~/synched"
alias ddtruefire="cd ~/synched/truefire"
alias ddvega="cd ~/data/vega-web"
alias ddversicherung="cd ~/data/acts/versicherung"
alias ddvertraege="cd ~/data/verträge-privat"

alias ddwork="cd ~/work"

alias vi="nvim"
alias vim="nvim"
alias hgrep="history | grep"

# allow case insensitive searches in less by default and allow ANSI characters
alias less="less -Ri"

# generate a long password and copy it to clipboard
# example 4PQwZ8y2p7e0g1L1ZxOPHhciCUcYmj0V39jBLdLN0ixJrqbywZ
alias pwgen="apg -a1 -n1 -m50 -d -M NCL |clipcopy"

# work on things
alias dotfiles="code ~/.dotfiles"
alias dotfilessecret="code ~/.dotfiles-secret"
alias homenet="code ~/work/homenet"

# ansible
alias ap="ansible-playbook"

# ansible-vault
alias avv="ansible-vault view"
alias ansible-vault="EDITOR='code --wait' ansible-vault"
alias ave="ansible-vault edit"

# apt
alias acs="apt-cache search"
alias agup="sudo apt-get update"
alias agupg="sudo apt-get upgrade"
alias ali="apt list --installed"
alias alu="apt list --upgradable"
alias ai="sudo apt install"

# ctop - docker container top
alias ctop="docker run -ti --name ctop --rm -v /var/run/docker.sock:/var/run/docker.sock quay.io/vektorlab/ctop:latest"

# docker
alias dps="docker ps"

# docker-compose
alias dc="docker compose"
alias dcud="docker compose up -d"
alias dcs="docker compose stop"
alias dcl="docker compose logs"
alias dct="docker compose top"

# emacs
alias em="emacs -nw"



# xclip
xclip_command=$(which xclip)
 if [ -x "$xclip_command" ] ;  then
    alias clip="xclip -selection clipboard"
 fi



# Firefox
# /Applications/Firefox.app/Contents/MacOS/firefox-bin -h
alias ffandy='nohup firefox -P "Andy" >/dev/null 2>&1 &'
#alias ffacme='nohup firefox -P "acme" >/dev/null 2>&1 &'


alias goclip="gopass show -c"



# helm
alias hdp="helm del --purge"
alias hls="helm ls"
alias hru="helm repo update"


alias magit='emacs -nw --eval "(magit-status)"'




# kubectl
alias kc="kubectl"
alias kcuc="kubectl config use-context"


# ledger
alias aledger="ledger -f $HOME/data/ledger/all.ledger"
alias pledger="ledger -f $HOME/data/ledger/all-private.ledger"
alias bledger="ledger -f $HOME/data/ledger/all-business.ledger"
alias hl23="hledger -f $HOME/data/ledger/2023-all.ledger"



# minikube
# alias mks="minikube start --cpus=4 --memory=8192 --vm-driver=hyperkit"
# alias mkdb="minikube dashboard"
# alias mksl="minikube service list"
#alias mkde="eval $(minikube docker-env)"

# mr
alias mr="mr -j8"
#alias mru="mr update"
alias mrf="mr fetch"

# nautilus (gnome)
alias n='nohup nautilus >/dev/null 2>&1 &'

# screen
alias sn='screen -S'  # sn for screen new
alias sl='screen -ls' # sl for screen list
alias sr='screen -x'  # sr for screen resume
function sk() {
  # sk for screen kill
  screen -S "$1" -X quit
}

alias youtube-dl-mp3="youtube-dl --extract-audio --audio-format mp3"


# open special files



# vagrant
#alias vagrant='docker run -it --rm  \
#    -e LIBVIRT_DEFAULT_URI -v \
#    /var/run/libvirt/:/var/run/libvirt/ \
#    -v ~/.vagrant.d:/.vagrant.d \
#    -v $(pwd):$(pwd) \
#    -w $(pwd) \
#    vagrantlibvirt/vagrant-libvirt:latest \
#    vagrant'


alias d30="date --date=\"30 days\""
alias bmi="open ~/data/health-fitness/nutrition/BMI_Berechnung.ods"




