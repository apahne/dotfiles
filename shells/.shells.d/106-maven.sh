
# initialize after sdkman!


mvn_command=$(which mvn)

if [ -x "$mvn_command" ] ;  then

  #export MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"

  alias mci="mvn clean install"
  alias mcp="mvn clean package"
  alias mct="echo \"mcv - mvn verify\" ;mvn verify"
  alias mcv="mvn clean verify"
  alias mvu="mvn versions:display-dependency-updates"
  alias mvpu="mvn org.codehaus.mojo:versions-maven-plugin:display-property-updates"
  alias mplu="mvn versions:display-plugin-updates"

fi


