# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

unsetopt bg_nice            # Don't run all background jobs at a lower priority.
unsetopt share_history      # do not share history across terminal sessions

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
ZSH_CUSTOM=$HOME/.dotfiles/zsh-custom
ZSH_THEME="andy-robbyrussell"
#ZSH_THEME="andy-agnoster"
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.dotfiles/zsh-custom/plugins/
plugins=(
    git
    git-extras
    httpie
    kubectl
    z
)

. "$HOME/.locale-settings"

# TODO check zsh-completions
# enable oh-my-zsh
source $ZSH/oh-my-zsh.sh

# case-sensitive completion
CASE_SENSITIVE="true"

#  hyphen-insensitive completion. Case sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"



# Highlights
#
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
ZSH_HIGHLIGHT_PATTERNS=('rm -rf *' 'fg=white,bold,bg=red')

# history
#
export HISTSIZE=32768;
export HISTFILESIZE=$HISTSIZE;
export HISTCONTROL=ignoredups;
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help";

# command execution time stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# some ZSH only aliases
alias -g L="|less"
alias -g G="|grep"


# source other files
[[ -d "$HOME/.shells.d" ]] && for f in "$HOME"/.shells.d/* ; do source $f; done
[[ -d "$HOME/.zsh.d" ]] && for f in "$HOME"/.zsh.d/* ; do source $f; done

# added with/for podman completion - not sure if generally required
compinit
autoload -U +X bashcompinit && bashcompinit

