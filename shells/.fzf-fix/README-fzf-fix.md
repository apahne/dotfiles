

apt package of fzf is kind of imcomplete (as of 2021-08), see 
    https://bugs.launchpad.net/ubuntu/+source/fzf/+bug/1928670

Fix

cp /usr/share/doc/fzf/examples/*.bash $HOME/.dotfiles/shells/.fzf-fix
cp /usr/share/doc/fzf/examples/*.zsh $HOME/.dotfiles/shells/.fzf-fix


