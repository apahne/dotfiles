
# kubectl completions
KUBECTL=$(which kubectl)
if [ -x "$KUBECTL" ] ; then
    source <("$KUBECTL" completion zsh)
fi

