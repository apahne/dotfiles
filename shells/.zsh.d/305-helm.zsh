
# helm completions
HELM=$(which helm)
if [ -x "$HELM" ] ; then
    source <(helm completion zsh)
fi

