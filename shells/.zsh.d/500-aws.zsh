

aws_zsh_completer=$(which "aws_zsh_completer.sh" 2>/dev/null )
if [[ -f "$aws_zsh_completer" ]] ; then
    source "$aws_zsh_completer"
fi
