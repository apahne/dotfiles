# Setup fzf

# apt package is currently broken, so manual fix
# see .dotfiles/shells/.fzf-fix/README-fzf-fix.md
[[ $- == *i* ]] && source "$HOME/.dotfiles/shells/.fzf-fix/completion.zsh" 2> /dev/null

# apply fix
# https://github.com/junegunn/fzf/issues/2790
# https://bugs.launchpad.net/ubuntu/+source/fzf/+bug/1928670
# still required as of 2023-06
source "$HOME/.dotfiles/shells/.fzf-fix/key-bindings.zsh"




