
# podman completion
mkdir -p ~/.zsh/completion
fpath=(~/.zsh/completion $fpath)


# podman
path_to_podman=$(which podman)
if [ -x "$path_to_podman" ] ;  then
    podman completion zsh -f "${fpath[1]}/_podman"
fi

