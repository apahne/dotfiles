
# find out, which `head` command to use for the gopass tab completion initialization
# on Mac it's `ghead` out of the package coreutils
# on Linux it's the regular `head` command
head_cmd="head"
if [ "$(uname)" = "Darwin" ]; then
    head_cmd=ghead
fi

# gopass completions
gopass_cmd=$(which gopass)
if [ -x "$gopass_cmd" ] ; then
    source <(gopass completion zsh | $head_cmd -n -1 | tail -n +2)
    compdef _gopass gopass
fi
