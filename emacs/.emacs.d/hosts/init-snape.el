(provide 'init-snape)

 (defun org-babel-tangle-config-snape ()
    (when (string-equal (buffer-file-name)
	    (expand-file-name "~/.dotfiles/emacs/.emacs.d/hosts/init-snape.el.org"))
    (let ((org-config-babel-evaluate nil))
    (org-babel-tangle))))

(add-hook 'org-mode-hook
	 (lambda ()
	   (add-hook 'after-save-hook 'org-babel-tangle-config-snape)))


