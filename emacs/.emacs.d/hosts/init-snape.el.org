#+TITLE: Emacs Configuration - Snape Only
#+PROPERTY: header-args:emacs-lisp :tangle ./init-snape.el

* Integration - Make this work

When adding this host for the very first time:
 * replace the host name *four* times in the following block
 * evaluate the block manually for one time
 * save the file and make sure it has been tangled
 * git commit

*Do not remove this block!*
 
#+begin_src emacs-lisp :results none

 (provide 'init-snape)

 (defun org-babel-tangle-config-snape ()
    (when (string-equal (buffer-file-name)
	    (expand-file-name "~/.dotfiles/emacs/.emacs.d/hosts/init-snape.el.org"))
    (let ((org-config-babel-evaluate nil))
    (org-babel-tangle))))

(add-hook 'org-mode-hook
	 (lambda ()
	   (add-hook 'after-save-hook 'org-babel-tangle-config-snape)))

#+end_src



* Test

#+begin_src emacs-lisp


#+end_src


