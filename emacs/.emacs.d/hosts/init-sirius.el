(provide 'init-sirius)

 (defun org-babel-tangle-config-sirius ()
    (when (string-equal (buffer-file-name)
	    (expand-file-name "~/.dotfiles/emacs/.emacs.d/hosts/init-sirius.el.org"))
    (let ((org-config-babel-evaluate nil))
    (org-babel-tangle))))

(add-hook 'org-mode-hook
	 (lambda ()
	   (add-hook 'after-save-hook 'org-babel-tangle-config-sirius)))

(set-language-environment "UTF-8")
(setq default-process-coding-system '(utf-8 . utf-8))

;; default font
 (set-face-attribute 'default nil :font "Fira Code Retina" :height 130)

 ;; Set the fixed pitch face
 (set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 130)

 ;; Set the variable pitch face
 (set-face-attribute 'variable-pitch nil :font "Cantarell" :height 140 :weight 'regular)
