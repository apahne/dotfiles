(defun is-macos ()
  "Return true if system is darwin-based (Mac OS X)"
  (string-equal system-type "darwin"))


(defun my-counsel-ag ()
  "ag search in current directory (instead of whole git root)"
  (interactive)
  (counsel-ag "" default-directory))

(defun org-babel-tangle-config ()
    (when (string-equal (buffer-file-name)
	    (expand-file-name "~/.dotfiles/emacs/.emacs.d/init.el.org"))
    (let ((org-config-babel-evaluate nil))
    (org-babel-tangle))))

(add-hook 'org-mode-hook
	 (lambda ()
	   (add-hook 'after-save-hook #'org-babel-tangle-config)))

(defun reload-dotemacs ()
	(interactive)
	(load-file "~/.emacs.d/init.el"))
    (global-set-key (kbd "C-c <f12>") 'reload-dotemacs)

;(server-start)

;; Keep transient cruft out of ~/.emacs.d/
(setq user-emacs-directory "~/.cache/emacs/"
    url-history-file (expand-file-name "url/history" user-emacs-directory)
    projectile-known-projects-file (expand-file-name "projectile-bookmarks.eld" user-emacs-directory))

(setq warning-minimum-level :emergency)

;; Keep customization settings in a temporary file (thanks Ambrevar!)
(setq custom-file
    (if (boundp 'server-socket-dir)
        (expand-file-name "custom.el" server-socket-dir)
        (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))
(load custom-file t)

(setq backup-directory-alist '(("." . "~/.cache/emacs/backups/")))

(make-directory (expand-file-name "~/.cache/emacs/auto-saves") t)
(setq auto-save-file-name-transforms
  `((".*" "~/.cache/emacs/auto-saves/" t)))

(recentf-mode 1)

(setq recentf-max-menu-items 120)
(setq recentf-max-saved-items nil)

(setq-default recent-save-file "~/.cache/emacs/recentf")
(add-to-list 'recentf-exclude
        (expand-file-name "~/.cache/emacs"))


(run-at-time nil (* 5 60) 'recentf-save-list)

(let ((path (shell-command-to-string ". ~/.bashrc; . ~/.dotfiles/shells/.shells.d/002-path.sh; echo -n $PATH")))
(setenv "PATH" path)
(setq exec-path
        (append
        (split-string-and-unquote path ":")
        exec-path)))

(add-to-list 'exec-path (expand-file-name "~/.sdkman/candidates/java/current/bin"))

(setenv "LEDGER_FILE" (expand-file-name "~/data/ledger/ledgers/all.ledger"))

(defun eshell-load-bash-aliases ()
  "Read Bash aliases and add them to the list of eshell aliases."
  ;; Bash needs to be run - temporarily - interactively
  ;; in order to get the list of aliases.
    (with-temp-buffer
      (call-process "bash" nil '(t nil) nil "-ci" "alias")
      (goto-char (point-min))
      (while (re-search-forward "alias \\(.+\\)='\\(.+\\)'$" nil t)
        (eshell/alias (match-string 1) (match-string 2)))))
;; We only want Bash aliases to be loaded when Eshell loads its own aliases,
;; rather than every time `eshell-mode' is enabled.
(add-hook 'eshell-alias-load-hook 'eshell-load-bash-aliases)

(setq pixel-scroll-precision-large-scroll-height 40.0)

;; scroll one line at a time (less "jumpy" than defaults)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling

(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse

(setq scroll-step 1) ;; keyboard scroll one line at a time

;; do not create lockfiles
(setq create-lockfiles nil)

;; "y" and "n" instead of "yes" and "no"
(fset 'yes-or-no-p 'y-or-n-p)

;; open file at last edit location
(save-place-mode 1)

;; Do not warn when opening symlinked files
(setq vc-follow-symlinks t)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Package management
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(
	 ("org"   . "http://orgmode.org/elpa/")
	 ("gnu"   . "http://elpa.gnu.org/packages/")
	 ("melpa-stable" . "https://stable.melpa.org/packages/")
	 ("melpa" . "https://melpa.org/packages/")))
(package-initialize)

;; Bootstrap `use-package`
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package dashboard
    :ensure t
    :config
    (dashboard-setup-startup-hook))
(setq dashboard-items '((recents  . 12)
                        (projects . 12)
                        (bookmarks . 5)
                        (registers . 5)))

(setq dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name)

;; Set the banner
;;(setq dashboard-startup-banner nil)
;;(setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
;; Value can be
;; 'official which displays the official emacs logo
;; 'logo which displays an alternative emacs logo
;; 1, 2 or 3 which displays one of the text banners
;; "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever image/text you would prefer

;; Content is not centered by default. To center, set
;;(setq dashboard-center-content t)

(defun er-switch-to-previous-buffer ()
    "Switch to previously open buffer.
    Repeated invocations toggle between the two most recently open buffers."
    (interactive)
    (switch-to-buffer (other-buffer (current-buffer) 1)))

;; source https://github.com/bbatsov/prelude
(defun rename-file-and-buffer ()
  "Rename the current buffer and file it is visiting."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (message "Buffer is not visiting a file!")
      (let ((new-name (read-file-name "New name: " filename)))
        (cond
         ((vc-backend filename) (vc-rename-file filename new-name))
         (t
          (rename-file filename new-name t)
          (set-visited-file-name new-name t t)))))))

(global-set-key (kbd "C-c r")  'rename-file-and-buffer)

;; Minimal UI
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(tooltip-mode    -1)
(menu-bar-mode   -1)

(use-package diminish)


;; window size on startup
(add-to-list 'default-frame-alist '(height . 50))
(add-to-list 'default-frame-alist '(width . 120))

;; default font
(set-face-attribute 'default nil :font "Fira Code Retina" :height 116)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 115)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Cantarell" :height 123 :weight 'regular)

; modus-operandi good for working with org-mode and long-ish texts
; (use-package modus-themes :ensure)

(use-package doom-themes
    :config
    ;; Global settings (defaults)
    (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
    (load-theme 'doom-one-light t)

    ;; Enable flashing mode-line on errors
    (doom-themes-visual-bell-config)

    ;; Enable custom neotree theme (all-the-icons must be installed!)
    (doom-themes-neotree-config)
    ;; or for treemacs users
    (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
    (doom-themes-treemacs-config)

    ;; Corrects (and improves) org-mode's native fontification.
    (doom-themes-org-config))

(use-package doom-modeline
    :ensure t
    :init (doom-modeline-mode 1)
    :custom ((doom-modeline-height 10)))

(use-package hide-mode-line)

;; All The Icons
;; need to run M-x all-the-icons-install-fonts to enable!
(use-package all-the-icons :ensure t)

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package helm
  :ensure t
  :init
  (setq helm-M-x-fuzzy-match t
  helm-mode-fuzzy-match t
  helm-buffers-fuzzy-matching t
  helm-recentf-fuzzy-match t
  helm-locate-fuzzy-match t
  helm-semantic-fuzzy-match t
  helm-imenu-fuzzy-match t
  helm-completion-in-region-fuzzy-match t
  helm-candidate-number-list 150
  helm-split-window-in-side-p t
  helm-move-to-line-cycle-in-source t
  helm-echo-input-in-header-line t
  helm-autoresize-max-height 0
  helm-autoresize-min-height 20)
  :config
  (helm-mode 1))

(setq helm-mini-default-sources '(helm-source-buffers-list
				  helm-source-recentf
				  helm-source-bookmarks
				  helm-source-buffer-not-found))

(use-package helm-swoop
  :ensure t
  :bind (("M-m" . helm-swoop)
	 ("M-M" . helm-swoop-back-to-last-point))
  :init
  (bind-key "M-m" 'helm-swoop-from-isearch isearch-mode-map))

(use-package helm-ag
  :ensure helm-ag
  :bind ("M-p" . helm-projectile-ag)
  :commands (helm-ag helm-projectile-ag)
  :init (setq helm-ag-insert-at-point 'symbol
	helm-ag-command-option "--path-to-ignore ~/.agignore"))

;; Which Key
(use-package which-key
  :ensure t
  :init
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+")
  (setq which-key-idle-delay 0.3)
  :config
  (which-key-mode 1)
  )

;  (use-package smex
;    :ensure t
;    :bind ("M-x" . smex))

(use-package ivy
   :diminish
   :bind (("C-s" . swiper)
	  :map ivy-minibuffer-map
	  ("TAB" . ivy-alt-done)
	  ("C-l" . ivy-alt-done)
	  ("C-j" . ivy-next-line)
	  ("C-k" . ivy-previous-line)
	  :map ivy-switch-buffer-map
	  ("C-k" . ivy-previous-line)
	  ("C-l" . ivy-done)
	  ("C-d" . ivy-switch-buffer-kill)
	  :map ivy-reverse-i-search-map
	  ("C-k" . ivy-previous-line)
	  ("C-d" . ivy-reverse-i-search-kill))
   :config
   (ivy-mode 1))

 (use-package swiper :ensure t)

 ;; Projectile
 (use-package projectile
   :diminish projectile-mode
   :config (projectile-mode)
   :custom ((projectile-completion-system 'ivy))
   :bind-keymap
   ("C-c p" . projectile-command-map)
   :init
   (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history)))

 (use-package counsel-projectile
   :config (counsel-projectile-mode))

(use-package org
:defer t
:config
(setq org-ellipsis " ▾"
    org-hide-emphasis-markers t
    org-src-fontify-natively t
    org-src-tab-acts-natively t
    org-edit-src-content-indentation 0
    org-hide-block-startup nil
    org-src-preserve-indentation nil
    org-startup-folded 'content
    org-cycle-separator-lines 2
    org-confirm-babel-evaluate nil))


(org-babel-do-load-languages 'org-babel-load-languages'(
  (shell . t)
  (python . t)
  (java . t)
  (R . t)
  (ruby . t)
  (ocaml . t)
  (sqlite . t)
  (screen . t)
  (plantuml . t)
  (dot . t)
  (org . t)
  (makefile . t)))

(setq org-plantuml-jar-path (expand-file-name "~/bin/plantuml.jar"))

;;  (use-package org-superstar)
;;  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))


(use-package org-superstar
  :after org
  :hook (org-mode . org-superstar-mode)
  :custom
  (org-superstar-remove-leading-stars t)
  (org-superstar-headline-bullets-list '("◉" "○" "●" "○" "●" "○" "●")))

;; Replace list hyphen with dot
;; (font-lock-add-keywords 'org-mode
;;                         '(("^ *\\([-]\\) "
;;                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

(when (is-macos)
  (setq
   mac-right-command-modifier 'super
   mac-command-modifier 'super
   mac-option-modifier 'meta
   mac-left-option-modifier 'meta
   mac-right-option-modifier 'meta
   mac-right-option-modifier 'nil))(dolist (face '((org-document-title . 1.6)
		    (org-level-1 . 1.5)
                (org-level-2 . 1.4)
                (org-level-3 . 1.3)
                (org-level-4 . 1.2)
                (org-level-5 . 1.1)
                (org-level-6 . 1.0)
                (org-level-7 . 1.0)
                (org-level-8 . 1.0)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

;; Make sure org-indent face is available
(require 'org-indent)

;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

;; TODO: Others to consider
;; '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
;; '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
;; '(org-property-value ((t (:inherit fixed-pitch))) t)
;; '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
;; '(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
;; '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
;; '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))

;; adjust how the outline is displayed in org mode files
(setq org-indent-mode-turns-on-hiding-stars nil
      org-startup-indented t
			org-adapt-indentation t
      org-indent-indentation-per-level 2)

;; allow to set a scale factor on inined images
(setq org-image-actual-width nil)

(defun pah/org-start-presentation ()
  (interactive)
  (hide-mode-line-mode 1)
  (org-tree-slide-mode 1)
  (setq text-scale-mode-amount 3)
  (text-scale-mode 1))

(defun pah/org-end-presentation ()
  (interactive)
  (text-scale-mode 0)
  (hide-mode-line-mode 0)
  (org-tree-slide-mode 0))

(use-package org-tree-slide
  :defer t
  :after org
  :commands org-tree-slide-mode
  :config
  (evil-define-key 'normal org-tree-slide-mode-map
    (kbd "q") 'pah/org-end-presentation
    (kbd "<f10>") 'org-tree-slide-move-next-tree
    (kbd "<f9>") 'org-tree-slide-move-previous-tree)
  (setq org-tree-slide-slide-in-effect nil
        org-tree-slide-activate-message "Presentation started."
        org-tree-slide-deactivate-message "Presentation ended."
        org-tree-slide-header t))

(setq org-file-apps
    '(("\\.docx\\'" . default)
      ("\\.mm\\'" . default)
      ("\\.x?html?\\'" . default)
      ;("\\.pdf\\'" . default)
      ("\\.pdf\\'" . "setsid -w xdg-open \"%s\"")
      ("\\.odt\\'" . "soffice \"%s\"")
      (Auto-Mode . emacs)))


(when (is-macos)
  (setq org-file-apps
    '(("\\.docx\\'" . default)
      ("\\.mm\\'" . default)
      ("\\.x?html?\\'" . default)
      ("\\.pdf\\'" . default)
      ("\\.odt\\'" . "soffice \"%s\"")
      (Auto-Mode . emacs))))

; version 9.5 reoved the default key binding for "clear cell", so let's reintroduce it
(define-key org-mode-map (kbd "C-c SPC") 'org-table-blank-field)

(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell :results verbatim :exports results"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("pl" . "src plantuml :file file.png :exports results"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))


; display generated images (e.g platuml diagrams) directly after it was generated
(add-hook 'org-babel-after-execute-hook
          (lambda ()
            (when org-inline-image-overlays
              (org-redisplay-inline-images))))

(use-package neotree
  :ensure t
  :init
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow)))

(use-package magit
	:custom
	(magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;(use-package evil-magit
;  :after magit)


(with-eval-after-load 'magit
	(setq magit-repository-directories
	'(;; Directory containing project root directories
		("~/data/"      . 2)
		("~/data/acts"      . 2)
		("~/data/_attic"      . 2)
		("~/projects/"      . 2)
		("~/synched/"      . 2)
		;; Specific project root directory
		("~/.dotfiles/" . 1)
		("~/.dotfiles-secret/" . 1)
		("~/work/puma/docs" . 1)
		("~/work/puma/plm-events-workshop" . 1)
		)))

(with-eval-after-load 'projectile
	(when (require 'magit nil t)
	  (mapc #'projectile-add-known-project
		(mapcar #'file-name-as-directory (magit-list-repos)))
	  ;; Optionally write to persistent `projectile-known-projects-file'
	  (projectile-save-known-projects)))

(use-package csv-mode
  :ensure t
  :init
  :config
  )

(use-package yaml-mode
  :ensure t)

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

;; Custom keybinding
(use-package general
  :ensure t
  :config (general-define-key
  :states '(normal visual insert emacs)
  :prefix "SPC"
  :non-normal-prefix "M-SPC"
	:keymaps 'override

  "TAB" '(er-switch-to-previous-buffer :which-key "previous buffer")
  "SPC" '(counsel-M-x :which-key "M-x")

  ;; Files
  "f" '(nil :which-key "file")
  "fa"  '(counsel-ag :which-key "ag")
  "fd"  '(my-counsel-ag :which-key "ag in current directory")
  "ff"  '(counsel-find-file :which-key "find")
  "fi"  '(insert-file :which-key "insert")
  "fr"  '(counsel-recentf :which-key "recent")
  "fz"  '(counsel-fzf :which-key "fzf")

  ;; Jump
  "j" '(nil :which-key "jump")
  "jb"  '(bookmark-jump :which-key "bookmark")
  "jh"  '(goto-last-change :which-key "back")
  "jl"  '(goto-line :which-key "line")

  ;; Project
  "p" '(nil :which-key "project")
  "pa"  '(counsel-projectile-ag :which-key "ag project files")
  "pp"  '(counsel-projectile-switch-project :which-key "projects")
  "pf"  '(counsel-projectile-find-file :which-key "project files")

  ;; Buffer
  "b" '(nil :which-key "buffer")
  "bb"  '(helm-mini :which-key "helm mini")
  "bd"  '(kill-current-buffer :which-key "kill current buffer")
  "bn"  '(evil-buffer-new :which-key "new")

  ;; Window
  "w" '(nil :which-key "window")
  "ww"  '(other-window :which-key "other")
  "wl"  '(windmove-right :which-key "right")
  "wh"  '(windmove-left :which-key "left")
  "wk"  '(windmove-up :which-key "up")
  "wj"  '(windmove-down :which-key "bottom")
  "w/"  '(split-window-right :which-key "split right")
  "w-"  '(split-window-below :which-key "split bottom")
  "wd"  '(delete-window :which-key "delete")
  "wD"  '(delete-other-windows :which-key "delete others")
  "wm"  '(minimize-window :which-key "minimize")
  "wM"  '(maximize-window :which-key "maximize")

  ;; Org
  "o" '(nil :which-key "org")
  "oo"  '(counsel-org-goto :which-key "org outline search")
  "oO"  '(counsel-org-goto-all :which-key "org outline search all")
  "oc"  '(org-cut-subtree :which-key "cut subtree")
  "op"  '(org-paste-subtree :which-key "paste subtree")
  "oy"  '(org-copy-subtree :which-key "yank (copy) subtree")

  ;; Searching
  "s" '(nil :which-key "search")
  "sa"  '(counsel-ag :which-key "ag")
  "sj"  '(counsel-org-goto :which-key "org outline search")
  "sr"  '(swiper-isearch :which-key "regexp")
  "ss"  '(swiper :which-key "swiper")
  "sp"  '(counsel-projectile-ag :which-key "ag project files")

  ;; Toggle
  "t" '(nil :which-key "toggle")
  "td"  '(org-tree-slide-mode :which-key "slide deck")
  "tl"  '(display-line-numbers-mode :which-key "line numbers")
  "tn"  '(org-toggle-narrow-to-subtree :which-key "narrow org subtree")
  "tt"  '(counsel-load-theme :which-key "theme")
  "ts"  '(hydra-text-scale/body :which-key "text scale")
  "ti"  '(org-toggle-inline-images :which-key "org inline images")
  "tv"  '(visual-line-mode :which-key "visual line mode")

  ;; Insert
  "i" '(nil :which-key "insert")
  "ia"  '(all-the-icons-insert :which-key "all-the-icons")
  "ib"  '(insert-buffer :which-key "buffer")
  "if"  '(insert-file :which-key "file")
  "ii"  '(ivy-yasnippet :which-key "snippet")
  "il" '(org-insert-link :which-key "link")

  ;; Links
  "l" '(nil :which-key "Link")
  "ll" '(org-insert-link :which-key "insert")

   ;; Git - Magit
  "g" '(nil :which-key "git")
  "gg"  '(magit-status :which-key "magit status")

  ;; Help
  "h" '(nil :which-key "help")
  "hc"  '(Info-goto-emacs-command-node :which-key "help command/variable name")
	

))

(evil-define-key '(normal insert visual) org-mode-map (kbd "C-S-K") 'org-previous-visible-heading)
(evil-define-key '(normal insert visual) org-mode-map (kbd "C-S-J") 'org-next-visible-heading)
(evil-define-key '(normal insert visual) org-mode-map (kbd "C-S-H") 'org-backward-heading-same-level)
(evil-define-key '(normal insert visual) org-mode-map (kbd "C-S-L") 'org-forward-heading-same-level)
(evil-define-key '(normal insert visual) org-mode-map (kbd "C-S-U") 'outline-up-heading)

(evil-define-key '(normal insert visual) org-mode-map (kbd "M-j") 'org-metadown)
(evil-define-key '(normal insert visual) org-mode-map (kbd "M-k") 'org-metaup)

(evil-define-key '(normal visual) org-mode-map (kbd ",n") 'org-toggle-narrow-to-subtree)

(define-key evil-normal-state-map (kbd ",w") 'save-buffer)
(define-key evil-normal-state-map (kbd ",r") 'org-babel-execute-buffer)

(use-package hydra)

(defhydra hydra-text-scale (:timeout 5)
  "scale text"
  ("j" text-scale-decrease "decrease")
  ("k" text-scale-increase "increase")
  ("q" nil "quit" :exit t))

(setq scroll-step            1
      scroll-conservatively  10000)

(global-set-key [next]
    (lambda () (interactive)
      (condition-case nil (scroll-up)
        (end-of-buffer (goto-char (point-max))))))
  
(global-set-key [prior]
  (lambda () (interactive)
    (condition-case nil (scroll-down)
      (beginning-of-buffer (goto-char (point-min))))))

(use-package markdown-mode
  :pin melpa-stable
  :mode "\\.md\\'"
  :config
  (setq markdown-command "marked")
  (defun dw/set-markdown-header-font-sizes ()
    (dolist (face '((markdown-header-face-1 . 1.4)
                    (markdown-header-face-2 . 1.3)
                    (markdown-header-face-3 . 1.2)
                    (markdown-header-face-4 . 1.1)
                    (markdown-header-face-5 . 1.0)))
      (set-face-attribute (car face) nil :weight 'normal :height (cdr face))))

  (defun dw/markdown-mode-hook ()
    (dw/set-markdown-header-font-sizes))

  (add-hook 'markdown-mode-hook 'dw/markdown-mode-hook))

(use-package ledger-mode
  :mode "\\.ledger\\'"
  :bind
  (:map ledger-mode-map
  ("TAB" . completion-at-point)))

;; Show matching parens
(setq show-paren-delay 0)
(show-paren-mode 1)

;; treat line wrapped lines as real lines when navigating
(global-visual-line-mode 1)

;; commenting
(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

;; Tabs
(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)

(when (version<= "26.0.50" emacs-version)
  (column-number-mode)
  (setq display-line-numbers-type 'relative)
  (add-hook 'prog-mode-hook #'display-line-numbers-mode)
  (add-hook 'text-mode-hook #'display-line-numbers-mode)
  ;; (add-hook 'org-mode-hook #'display-line-numbers-mode)
)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                erc-mode-hook
                treemacs-mode-hook
                eshell-mode-hook dired-mode-hook))
  (add-hook mode
            (lambda ()
              (display-line-numbers-mode 0))))

(use-package ivy-prescient
  :after counsel
  :config
  (ivy-prescient-mode 1))

; sort suggestions by length
(setq prescient-sort-length-enable nil)

(setq ivy-prescient-retain-classic-highlighting t)

; remember candidate frequencies between sessions
(prescient-persist-mode 1)

(use-package yasnippet
  :ensure t
  :config
  (setq yas-snippet-dirs '("~/data/snippets/yasnippet/"))
  (yas-global-mode 1))

(use-package ivy-yasnippet
  :bind ("C-c y" . ivy-yasnippet))

;(use-package auto-complete
; :ensure t
;  :config
;  (setq ac-auto-start nil)
;  (ac-set-trigger-key "C-SPC")
;)

(setq display-time-world-list'(
    ("EST" "Boston")
    ("GMT/Zulu" "GMT")
    ("Europe/Berlin" "Berlin")
    ("Asia/Kolkata" "Kolkata")
    ("Asia/Bangkok" "Bangkok")
    ("Asia/Ho_Chi_Minh" "Vietnam")
    ("Asia/Makassar" "Bali")
    ("Asia/Hong_Kong" "Hong Kong")
))

;; (setq display-time-world-time-format "%a, %d %b %I:%M %p %Z")
(setq display-time-world-time-format "%Z\t%a %d %b %R")   ; 24 h format

(setq user-full-name "Andy Pahne"
      user-mail-address "andy.pahne@protonmail.com "
      calendar-latitude 51.9412
      calendar-longitude 9.0986
      calendar-location-name "Blomberg, Germany")

(defvar host (substring (shell-command-to-string "hostname") 0 -1))
(defvar host-dir "~/.emacs.d/hosts/")
(add-to-list 'load-path host-dir)

(let ((init-host-feature (intern (concat "init-" host))))
  (require init-host-feature nil 'noerror ))

(use-package ox-reveal)

(setq org-reveal-root  (expand-file-name "~/projects/_other/reveal.js"))
