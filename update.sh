#!/bin/bash

# check required dependencies
type git >/dev/null 2>&1 || { echo >&2 "git not installed.  Aborting."; exit 1; }

# Get current dir (so run this script from anywhere)
export DOTFILES_DIR
DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "dotfiles dir = $DOTFILES_DIR"

# Update dotfiles itself
[ -d "$DOTFILES_DIR/.git" ] && git --work-tree="$DOTFILES_DIR" --git-dir="$DOTFILES_DIR/.git" pull origin master

